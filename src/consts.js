
export const ROUTES = {
	SELLER: '/sellers',
	SELLERLISTAPI: '/sellers-ranking',
	SELLERORDERSAPI: '/orders'
};

export const OMNI_URL = 'https://gestao-omni-dot-apt-bonbon-179602.appspot.com/v0';
